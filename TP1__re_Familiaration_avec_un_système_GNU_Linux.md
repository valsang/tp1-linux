# TP1 : (re)Familiaration avec un système GNU/Linux
## 0. Préparation de la machine
### Setup de deux machines Rocky Linux configurées de façon basique.
- un accès internet (via la carte NAT)
```
[kalop@node2 ~]$ ping 8.8.8.8
PING 8.8.8.8 (8.8.8.8) 56(84) bytes of data.
64 bytes from 8.8.8.8: icmp_seq=1 ttl=114 time=20.4 ms
64 bytes from 8.8.8.8: icmp_seq=2 ttl=114 time=18.8 ms
64 bytes from 8.8.8.8: icmp_seq=3 ttl=114 time=20.9 ms
64 bytes from 8.8.8.8: icmp_seq=4 ttl=114 time=20.0 ms
^C
--- 8.8.8.8 ping statistics ---
4 packets transmitted, 4 received, 0% packet loss, time 3005ms
rtt min/avg/max/mdev = 18.783/20.034/20.889/0.803 ms
[kalop@node2 ~]$ ping google.com
PING google.com (142.250.179.142) 56(84) bytes of data.
64 bytes from ams17s10-in-f14.1e100.net (142.250.179.142): icmp_seq=1 ttl=110 time=28.6 ms
64 bytes from ams17s10-in-f14.1e100.net (142.250.179.142): icmp_seq=2 ttl=110 time=27.7 ms
64 bytes from ams17s10-in-f14.1e100.net (142.250.179.142): icmp_seq=3 ttl=110 time=27.5 ms
^C
--- google.com ping statistics ---
3 packets transmitted, 3 received, 0% packet loss, time 2004ms
rtt min/avg/max/mdev = 27.510/27.936/28.604/0.478 ms
[kalop@node2 ~]$
```
-un accès à un réseau local 

```
[kalop@node1 ~]$ [kalop@node1 ~]$ ping node2.tp1.b2
PING node2.tp1.b2 (10.101.1.12) 56(84) bytes of data.
64 bytes from node2.tp1.b2 (10.101.1.12): icmp_seq=1 ttl=64 time=0.747 ms
64 bytes from node2.tp1.b2 (10.101.1.12): icmp_seq=2 ttl=64 time=0.981 ms
64 bytes from node2.tp1.b2 (10.101.1.12): icmp_seq=3 ttl=64 time=0.973 ms
64 bytes from node2.tp1.b2 (10.101.1.12): icmp_seq=4 ttl=64 time=1.08 ms
^C
--- node2.tp1.b2 ping statistics ---
4 packets transmitted, 4 received, 0% packet loss, time 3007ms
rtt min/avg/max/mdev = 0.747/0.944/1.077/0.124 ms
[kalop@node1 ~]$
```

```
[kalop@node2 ~]$ [kalop@node2 ~]$ ping node1.tp1.b2
PING node1.tp1.b2 (10.101.1.11) 56(84) bytes of data.
64 bytes from node1.tp1.b2 (10.101.1.11): icmp_seq=1 ttl=64 time=1.67 ms
64 bytes from node1.tp1.b2 (10.101.1.11): icmp_seq=2 ttl=64 time=0.914 ms
64 bytes from node1.tp1.b2 (10.101.1.11): icmp_seq=3 ttl=64 time=1.06 ms
^C
--- node1.tp1.b2 ping statistics ---
3 packets transmitted, 3 received, 0% packet loss, time 2002ms
rtt min/avg/max/mdev = 0.914/1.215/1.668/0.326 ms
[kalop@node2 ~]$
```
-utiliser 1.1.1.1 comme serveur DNS

```
[kalop@node1 ~]$ [kalop@node1 ~]$ dig ynov.com

; <<>> DiG 9.11.26-RedHat-9.11.26-4.el8_4 <<>> ynov.com
;; global options: +cmd
;; Got answer:
;; ->>HEADER<<- opcode: QUERY, status: NOERROR, id: 64220
;; flags: qr rd ra; QUERY: 1, ANSWER: 1, AUTHORITY: 0, ADDITIONAL: 1

;; OPT PSEUDOSECTION:
; EDNS: version: 0, flags:; udp: 1232
;; QUESTION SECTION:
;ynov.com.                      IN      A

;; ANSWER SECTION:
ynov.com.               8480    IN      A       92.243.16.143

;; Query time: 25 msec
;; SERVER: 1.1.1.1#53(1.1.1.1)
;; WHEN: Wed Sep 22 11:31:43 CEST 2021
;; MSG SIZE  rcvd: 53

[kalop@node1 ~]$
```
- la ligne suivante contient l'adresse ip de ynov.com
```
;; ANSWER SECTION:
ynov.com.               8480    IN      A       92.243.16.143
```
- la ligne suivante contient l'adresse ip du serveur dns qui nous a répondu
```
;; SERVER: 1.1.1.1#53(1.1.1.1)
```

- les machines doivent pouvoir se joindre par leurs noms respectifs

```
[kalop@node1 ~]$ [kalop@node1 ~]$ ping node2.tp1.b2
PING node2.tp1.b2 (10.101.1.12) 56(84) bytes of data.
64 bytes from node2.tp1.b2 (10.101.1.12): icmp_seq=1 ttl=64 time=0.747 ms
64 bytes from node2.tp1.b2 (10.101.1.12): icmp_seq=2 ttl=64 time=0.981 ms
64 bytes from node2.tp1.b2 (10.101.1.12): icmp_seq=3 ttl=64 time=0.973 ms
64 bytes from node2.tp1.b2 (10.101.1.12): icmp_seq=4 ttl=64 time=1.08 ms
^C
--- node2.tp1.b2 ping statistics ---
4 packets transmitted, 4 received, 0% packet loss, time 3007ms
rtt min/avg/max/mdev = 0.747/0.944/1.077/0.124 ms
[kalop@node1 ~]$
```

```
[kalop@node2 ~]$ [kalop@node2 ~]$ ping node1.tp1.b2
PING node1.tp1.b2 (10.101.1.11) 56(84) bytes of data.
64 bytes from node1.tp1.b2 (10.101.1.11): icmp_seq=1 ttl=64 time=1.67 ms
64 bytes from node1.tp1.b2 (10.101.1.11): icmp_seq=2 ttl=64 time=0.914 ms
64 bytes from node1.tp1.b2 (10.101.1.11): icmp_seq=3 ttl=64 time=1.06 ms
^C
--- node1.tp1.b2 ping statistics ---
3 packets transmitted, 3 received, 0% packet loss, time 2002ms
rtt min/avg/max/mdev = 0.914/1.215/1.668/0.326 ms
[kalop@node2 ~]$
```
- le pare-feu est configuré pour bloquer toutes les connexions exceptées celles qui sont nécessaires

```
[kalop@node1 ~]$ sudo firewall-cmd --list-all
public (active)
  target: default
  icmp-block-inversion: no
  interfaces: enp0s3 enp0s8
  sources:
  services: ssh
  ports: 22/tcp
  protocols:
  masquerade: no
  forward-ports:
  source-ports:
  icmp-blocks:
  rich rules:
[kalop@node1 ~]$
```

## I. Utilisateurs
### 1. Création et configuration
- Ajouter un utilisateur à la machine, qui sera dédié à son administration. Précisez des options sur la commande d'ajout pour que : 
- le répertoire home de l'utilisateur soit précisé explicitement, et se trouve dans /home
- le shell de l'utilisateur soit /bin/bash
```
[kalop@node1 ~]$ sudo useradd kalip -m -s /bin/bash
```
- Créer un nouveau groupe admins qui contiendra les utilisateurs de la machine ayant accès aux droits de root via la commande sudo

```
[kalop@node1 home]$ sudo groupadd admins
```

```
[kalop@node1 home]$ sudo cat /etc/sudoers
## Sudoers allows particular users to run various commands as
## the root user, without needing the root password.
##
## Examples are provided at the bottom of the file for collections
## of related commands, which can then be delegated out to particular
## users or groups.
##
## This file must be edited with the 'visudo' command.

## Host Aliases
## Groups of machines. You may prefer to use hostnames (perhaps using
## wildcards for entire domains) or IP addresses instead.
# Host_Alias     FILESERVERS = fs1, fs2
# Host_Alias     MAILSERVERS = smtp, smtp2

## User Aliases
## These aren't often necessary, as you can use regular groups
## (ie, from files, LDAP, NIS, etc) in this file - just use %groupname
## rather than USERALIAS
# User_Alias ADMINS = jsmith, mikem


## Command Aliases
## These are groups of related commands...

## Networking
# Cmnd_Alias NETWORKING = /sbin/route, /sbin/ifconfig, /bin/ping, /sbin/dhclient, /usr/bin/net, /sbin/iptables, /usr/bin/rfcomm, /usr/bin/wvdial, /sbin/iwconfig, /sbin/mii-tool

## Installation and management of software
# Cmnd_Alias SOFTWARE = /bin/rpm, /usr/bin/up2date, /usr/bin/yum

## Services
# Cmnd_Alias SERVICES = /sbin/service, /sbin/chkconfig, /usr/bin/systemctl start, /usr/bin/systemctl stop, /usr/bin/systemctl reload, /usr/bin/systemctl restart, /usr/bin/systemctl status, /usr/bin/systemctl enable, /usr/bin/systemctl disable

## Updating the locate database
# Cmnd_Alias LOCATE = /usr/bin/updatedb

## Storage
# Cmnd_Alias STORAGE = /sbin/fdisk, /sbin/sfdisk, /sbin/parted, /sbin/partprobe, /bin/mount, /bin/umount

## Delegating permissions
# Cmnd_Alias DELEGATING = /usr/sbin/visudo, /bin/chown, /bin/chmod, /bin/chgrp

## Processes
# Cmnd_Alias PROCESSES = /bin/nice, /bin/kill, /usr/bin/kill, /usr/bin/killall

## Drivers
# Cmnd_Alias DRIVERS = /sbin/modprobe

# Defaults specification

#
# Refuse to run if unable to disable echo on the tty.
#
Defaults   !visiblepw

#
# Preserving HOME has security implications since many programs
# use it when searching for configuration files. Note that HOME
# is already set when the the env_reset option is enabled, so
# this option is only effective for configurations where either
# env_reset is disabled or HOME is present in the env_keep list.
#
Defaults    always_set_home
Defaults    match_group_by_gid

# Prior to version 1.8.15, groups listed in sudoers that were not
# found in the system group database were passed to the group
# plugin, if any. Starting with 1.8.15, only groups of the form
# %:group are resolved via the group plugin by default.
# We enable always_query_group_plugin to restore old behavior.
# Disable this option for new behavior.
Defaults    always_query_group_plugin

Defaults    env_reset
Defaults    env_keep =  "COLORS DISPLAY HOSTNAME HISTSIZE KDEDIR LS_COLORS"
Defaults    env_keep += "MAIL PS1 PS2 QTDIR USERNAME LANG LC_ADDRESS LC_CTYPE"
Defaults    env_keep += "LC_COLLATE LC_IDENTIFICATION LC_MEASUREMENT LC_MESSAGES"
Defaults    env_keep += "LC_MONETARY LC_NAME LC_NUMERIC LC_PAPER LC_TELEPHONE"
Defaults    env_keep += "LC_TIME LC_ALL LANGUAGE LINGUAS _XKB_CHARSET XAUTHORITY"

#
# Adding HOME to env_keep may enable a user to run unrestricted
# commands via sudo.
#
# Defaults   env_keep += "HOME"

Defaults    secure_path = /sbin:/bin:/usr/sbin:/usr/bin

## Next comes the main part: which users can run what software on
## which machines (the sudoers file can be shared between multiple
## systems).
## Syntax:
##
##      user    MACHINE=COMMANDS
##
## The COMMANDS section may have other options added to it.
##
## Allow root to run any commands anywhere
root    ALL=(ALL)       ALL
%admins ALL=(ALL)       ALL

## Allows members of the 'sys' group to run networking, software,
## service management apps and more.
# %sys ALL = NETWORKING, SOFTWARE, SERVICES, STORAGE, DELEGATING, PROCESSES, LOCATE, DRIVERS

## Allows people in group wheel to run all commands
%wheel  ALL=(ALL)       ALL

## Same thing without a password
# %wheel        ALL=(ALL)       NOPASSWD: ALL

## Allows members of the users group to mount and unmount the
## cdrom as root
# %users  ALL=/sbin/mount /mnt/cdrom, /sbin/umount /mnt/cdrom

## Allows members of the users group to shutdown this system
# %users  localhost=/sbin/shutdown -h now

## Read drop-in files from /etc/sudoers.d (the # here does not mean a comment)
#includedir /etc/sudoers.d
```
- Ajouter votre utilisateur à ce groupe admins
```
[kalop@node1 home]$ sudo gpasswd -a kalip admins
```
### 2. SSH
- Assurez vous que la connexion SSH est fonctionnelle, sans avoir besoin de mot de passe.
```
PS C:\Users\rukab> ssh kalop@10.101.1.11
Activate the web console with: systemctl enable --now cockpit.socket

Last login: Wed Sep 22 12:08:57 2021
[kalop@node1 ~]$
```

## II. Partitionnement
### 1. Préparation de la VM
- Ajout de deux disques durs à la machine virtuelle, de 3Go chacun.

![](https://i.imgur.com/Gj6IO49.png)

### 2. Partitionnement
- Utilisez LVM pour :

- agréger les deux disques en un seul volume group
```
[kalop@node1 ~]$ sudo pvcreate /dev/sdb
[sudo] password for kalop:
  Physical volume "/dev/sdb" successfully created.
[kalop@node1 ~]$ sudo pvcreate /dev/sdc
  Physical volume "/dev/sdc" successfully created.
[kalop@node1 ~]$
```

```
[kalop@node1 ~]$ sudo vgcreate data /dev/sdb
  Volume group "data" successfully created
[kalop@node1 ~]$ sudo vgextend data /dev/sdc
  Volume group "data" successfully extended
[kalop@node1 ~]$ sudo vgs
  VG   #PV #LV #SN Attr   VSize  VFree
  data   2   0   0 wz--n-  5.99g 5.99g
  rl     1   2   0 wz--n- <7.00g    0
[kalop@node1 ~]$
```

- créer 3 logical volumes de 1 Go chacun formater ces partitions en ext4
```
[kalop@node1 ~]$ [kalop@node1 ~]$ sudo lvcreate -L 1G data -n lv_data_1
  Logical volume "lv_data_1" created.
[kalop@node1 ~]$ sudo lvcreate -L 1G data -n lv_data_2
  Logical volume "lv_data_2" created.
[kalop@node1 ~]$ sudo lvcreate -L 1G data -n lv_data_3
  Logical volume "lv_data_3" created.
[kalop@node1 ~]$ sudo lvs
  LV        VG   Attr       LSize   Pool Origin Data%  Meta%  Move Log Cpy%Sync Convert
  lv_data_1 data -wi-a-----   1.00g
  lv_data_2 data -wi-a-----   1.00g
  lv_data_3 data -wi-a-----   1.00g
  root      rl   -wi-ao----  <6.20g
  swap      rl   -wi-ao---- 820.00m
[kalop@node1 ~]$
```

```
[kalop@node1 ~]$ [kalop@node1 ~]$ sudo mkfs -t ext4 /dev/data/lv_data_1
mke2fs 1.45.6 (20-Mar-2020)
Creating filesystem with 262144 4k blocks and 65536 inodes
Filesystem UUID: 1c380cc2-1fc1-4c28-903f-db6323eb24c0
Superblock backups stored on blocks:
        32768, 98304, 163840, 229376

Allocating group tables: done
Writing inode tables: done
Creating journal (8192 blocks): done
Writing superblocks and filesystem accounting information: done

[kalop@node1 ~]$ sudo mkfs -t ext4 /dev/data/lv_data_2
mke2fs 1.45.6 (20-Mar-2020)
Creating filesystem with 262144 4k blocks and 65536 inodes
Filesystem UUID: bfe70f3f-3fff-43f2-b24c-81fc7e38d6f8
Superblock backups stored on blocks:
        32768, 98304, 163840, 229376

Allocating group tables: done
Writing inode tables: done
Creating journal (8192 blocks): done
Writing superblocks and filesystem accounting information: done

[kalop@node1 ~]$ sudo mkfs -t ext4 /dev/data/lv_data_3
mke2fs 1.45.6 (20-Mar-2020)
Creating filesystem with 262144 4k blocks and 65536 inodes
Filesystem UUID: 6cbd8425-3e82-4247-9943-5db12c194686
Superblock backups stored on blocks:
        32768, 98304, 163840, 229376

Allocating group tables: done
Writing inode tables: done
Creating journal (8192 blocks): done
Writing superblocks and filesystem accounting information: done

[kalop@node1 ~]$
```
- monter ces partitions pour qu'elles soient accessibles aux points de montage /mnt/part1, /mnt/part2 et /mnt/part3

```
[kalop@node1 ~]$ sudo mkdir /mnt/part1
[kalop@node1 ~]$ sudo mkdir /mnt/part2
[kalop@node1 ~]$ sudo mkdir /mnt/part3
[kalop@node1 ~]$ sudo mount /dev/data/lv_data_1 /mnt/part1
[kalop@node1 ~]$ sudo mount /dev/data/lv_data_2 /mnt/part2
[kalop@node1 ~]$ sudo mount /dev/data/lv_data_3 /mnt/part3
```

- Grâce au fichier /etc/fstab, faites en sorte que cette partition soit montée automatiquement au démarrage du système.

```
[kalop@node1 ~]$ nano /etc/fstab
[kalop@node1 ~]$ sudo nano /etc/fstab
[kalop@node1 ~]$ [kalop@node1 ~]$ cat /etc/fstab

#
# /etc/fstab
# Created by anaconda on Wed Sep 15 08:30:09 2021
#
# Accessible filesystems, by reference, are maintained under '/dev/disk/'.
# See man pages fstab(5), findfs(8), mount(8) and/or blkid(8) for more info.
#
# After editing this file, run 'systemctl daemon-reload' to update systemd
# units generated from this file.
#
/dev/mapper/rl-root     /                       xfs     defaults        0 0
UUID=cb5bca76-9ca0-458a-ab84-7e8a44b2280d /boot                   xfs     defaults        0 0
/dev/mapper/rl-swap     none                    swap    defaults        0 0
/dev/data/lv_data_1 /mnt/part1 ext4 defaults 0 0
/dev/data/lv_data_2 /mnt/part2 ext4 default 0 0
/dev/data/lv_data_3 /mnt/part3 ext4 default 0 0
[kalop@node1 ~]$
```
## III. Gestion de services
### 1. Interaction avec un service existant

- Parmi les services système déjà installés sur CentOS, il existe firewalld. Cet utilitaire est l'outil de firewalling de CentOS. Assurez-vous que :

- l'unité est démarrée
```
[kalop@node1 ~]$ [kalop@node1 ~]$ systemctl is-active firewalld
active
```
- l'unitée est activée (elle se lance automatiquement au démarrage)
```
[kalop@node1 ~]$ systemctl is-enabled firewalld
enabled
```

### 2. Création de service
#### A. Unité simpliste
- Créer un fichier qui définit une unité de service web.service dans le répertoire /etc/systemd/system.
```
[kalop@node2 system]$ sudo firewall-cmd --add-port=8888/tcp --permanent
success
[kalop@node2 system]$ cat web.service
[Unit]
Description=Very simple web service

[Service]
ExecStart=/bin/python3 -m http.server 8888

[Install]
WantedBy=multi-user.target
[kalop@node2 system]$
```
- Une fois le service démarré, assurez-vous que pouvez accéder au serveur web : avec un navigateur ou la commande curl sur l'IP de la VM, port 8888.
```
PS C:\Users\rukab> curl http://10.101.1.12:8888/


StatusCode        : 200
StatusDescription : OK
Content           : <!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
                    <html>
                    <head>
                    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
                    <title>Directory listing fo...
RawContent        : HTTP/1.0 200 OK
                    Content-Length: 942
                    Content-Type: text/html; charset=utf-8
                    Date: Sun, 26 Sep 2021 14:31:20 GMT
                    Server: SimpleHTTP/0.6 Python/3.6.8

                    <!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//...
Forms             : {}
Headers           : {[Content-Length, 942], [Content-Type, text/html; charset=utf-8], [Date, Sun, 26 Sep 2021 14:31:20
                    GMT], [Server, SimpleHTTP/0.6 Python/3.6.8]}
Images            : {}
InputFields       : {}
Links             : {@{innerHTML=bin@; innerText=bin@; outerHTML=<A href="bin/">bin@</A>; outerText=bin@; tagName=A;
                    href=bin/}, @{innerHTML=boot/; innerText=boot/; outerHTML=<A href="boot/">boot/</A>;
                    outerText=boot/; tagName=A; href=boot/}, @{innerHTML=dev/; innerText=dev/; outerHTML=<A
                    href="dev/">dev/</A>; outerText=dev/; tagName=A; href=dev/}, @{innerHTML=etc/; innerText=etc/;
                    outerHTML=<A href="etc/">etc/</A>; outerText=etc/; tagName=A; href=etc/}...}
ParsedHtml        : mshtml.HTMLDocumentClass
RawContentLength  : 942



PS C:\Users\rukab>
```
#### B. Modification de l'unité
- Créer un utilisateur web.
```
[kalop@node2 system]$ sudo useradd web -m -s /bin/bash
```
-  Vérifier le bon fonctionnement avec une commande curl
```
PS C:\Users\rukab> curl http://10.101.1.12:8888/srv/webatest/test.txt


StatusCode        : 200
StatusDescription : OK
Content           : test 42

RawContent        : HTTP/1.0 200 OK
                    Content-Length: 8
                    Content-Type: text/plain
                    Date: Sun, 26 Sep 2021 14:51:24 GMT
                    Last-Modified: Sun, 26 Sep 2021 14:49:26 GMT
                    Server: SimpleHTTP/0.6 Python/3.6.8

                    test 42

Forms             : {}
Headers           : {[Content-Length, 8], [Content-Type, text/plain], [Date, Sun, 26 Sep 2021 14:51:24 GMT],
                    [Last-Modified, Sun, 26 Sep 2021 14:49:26 GMT]...}
Images            : {}
InputFields       : {}
Links             : {}
ParsedHtml        : mshtml.HTMLDocumentClass
RawContentLength  : 8



PS C:\Users\rukab>
```